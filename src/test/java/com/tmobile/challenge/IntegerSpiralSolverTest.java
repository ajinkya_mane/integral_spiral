package com.tmobile.challenge;

import com.tmobile.challenge.logic.IntegerSpiralSolver;
import com.tmobile.challenge.logic.Solver;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;
import java.util.Map;

public class IntegerSpiralSolverTest {

    private Solver solver;

    @Before
    public void setUp() {
        solver = new IntegerSpiralSolver();
    }

    @Test(expected = IllegalArgumentException.class)
    public void noInput() {
        Map<String, String> input = new HashMap<String, String>();
        solver.solve(input);

    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidInput() {
        //Invalid input
        Map<String, String> input = new HashMap<String, String>();
        input.put("input.seed", "8");
        solver.solve(input);

    }

    @Test
    public void testIntegerSpiralSolver3() {
        //valid input
        Map<String, String> input = new HashMap<String, String>();
        input.put("input.seed", "8");
        input.put("input.spin", "left");
        solver.solve(input);
        Assert.assertEquals("  8:  7:  6,  1:  0:  5,  2:  3:  4", input.get("solution"));
    }

    @Test
    public void testIntegerSpiralSolver4() {
        Map<String, String> input = new HashMap<String, String>();
        input.put("input.seed", "8");
        input.put("input.spin", "right");
        solver.solve(input);
        Assert.assertEquals("  6:  7:  8,  5:  0:  1,  4:  3:  2", input.get("solution"));
    }

    @Test
    public void testIntegerSpiralSolver5() {
        //valid input
        Map<String, String> input = new HashMap<String, String>();
        input.put("input.seed", "0");
        input.put("input.spin", "left");
        solver.solve(input);
        Assert.assertEquals("  0", input.get("solution"));
    }

    @Test
    public void testIntegerSpiralSolver6() {
        Map<String, String> input = new HashMap<String, String>();
        input.put("input.seed", "0");
        input.put("input.spin", "right");
        solver.solve(input);
        Assert.assertEquals("  0", input.get("solution"));
    }

}
