/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('puzzlesQuizUi')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
