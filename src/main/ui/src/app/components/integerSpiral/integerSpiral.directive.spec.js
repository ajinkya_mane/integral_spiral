(function() {
  'use strict';

  /**
   * @todo Complete the test
   * This example is not perfect.
   * Test should check if MomentJS have been called
   */
  describe('directive integerSpiral', function() {
    // var $window;
    var el;
    var vm;

    beforeEach(module('puzzlesQuizUi'));

    beforeEach(inject(function($compile, $rootScope, puzzlesQuizService, $q) {

      el = angular.element('<integer-spiral/>');

      spyOn(puzzlesQuizService, 'getIntegerSpiralSolution').and.callFake(function() {
        return $q.when([{}, {}, {}, {}, {}, {}]);
      });

      $compile(el)($rootScope.$new());
      $rootScope.$digest();
      vm = el.isolateScope().intSpiral;

    }));

    it('should be compiled', function() {
      expect(el.html()).not.toEqual(null);
    });

    it('should have isolate scope object with instanciate members', function() {
      expect(vm).toEqual(jasmine.any(Object));

      expect(vm.showOutput).toEqual(jasmine.any(Boolean));

    });

  });
})();
