(function() {
  'use strict';

  angular
    .module('puzzlesQuizUi')
    .directive('integerSpiral', integerSpiral);

  /** @ngInject */
  function integerSpiral() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/integerSpiral/integerSpiral.html',
      scope: {},
      controller: IntegerSpiralController,
      controllerAs: 'intSpiral',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function IntegerSpiralController(puzzlesQuizService) {

      var vm = this;
      vm.showOutput=false;
      vm.states = ["left", "right"];
      vm.submit = function() {
        vm.showOutput=false;

        puzzlesQuizService.getIntegerSpiralSolution(vm.number, vm.spin)
        .then(function successCallBack(response) {
          vm.errorMessage= "";
          var result = response.data;
          var status = response.status;
          if(status == 200) {
            if(undefined != result) {
              var rows = result.split(",");
              var grid = new Array(rows.length);
              for (var i = 0; i < rows.length; i++) {
                  grid[i] = rows[i].split(":")
              }
              vm.showOutput=true;
              vm.grid = grid;
            } else {
              vm.errorMessage="Result is undefined!!!";
            }
          } else {
            vm.errorMessage=response.statusText;
          }

        })
        .catch( function errorCallBack(error) {
          vm.errorMessage="Failed to get response[Error Code: "+error.status+", Error Message: " + error.statusText +
           "]";
        });

      }

    }
  }

})();
