(function() {
  'use strict';

  describe('service puzzlesQuizService', function() {
    var puzzlesQuizService;
    var $httpBackend;

    beforeEach(module('puzzlesQuizUi'));
    beforeEach(inject(function(_puzzlesQuizService_, _$httpBackend_) {
      puzzlesQuizService = _puzzlesQuizService_;
      $httpBackend = _$httpBackend_;
    }));

    it('should be registered', function() {
      expect(puzzlesQuizService).not.toEqual(null);
    });

    describe('apiHost variable', function() {
      it('should exist', function() {
        expect(puzzlesQuizService.apiHost).not.toEqual(null);
      });
    });

    describe('getIntegerSpiralSolution function', function() {
      it('should exist', function() {
        expect(puzzlesQuizService.getIntegerSpiralSolution).not.toEqual(null);
      });

      it('should return data', function() {
        $httpBackend.when('GET',  puzzlesQuizService.apiHost + '/v1/integer-spiral?seed=0&spin=right').respond(200,
        "0");
        var data;
        puzzlesQuizService.getIntegerSpiralSolution("0", "right").then(function(response) {
          data = response.data;
        });
        $httpBackend.flush();
        expect(data).toEqual(jasmine.any(String));
        expect(data).toEqual("0");
      });

    });



  });
})();
