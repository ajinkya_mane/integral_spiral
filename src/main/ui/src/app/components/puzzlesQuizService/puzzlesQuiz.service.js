(function() {
  'use strict';

  angular
      .module('puzzlesQuizUi')
      .factory('puzzlesQuizService', puzzlesQuizService);

  /** @ngInject */
  function puzzlesQuizService($http, $window) {

	//var apiHost = 'http://localhost:50000';
    var apiHost = '/puzzles-quiz';
    var service = {
      apiHost: apiHost,
      getIntegerSpiralSolution: getIntegerSpiralSolution
    };

    return service;

    function getIntegerSpiralSolution(input, spin) {
      input = $window.encodeURIComponent(input);
      spin = $window.encodeURIComponent(spin);
      var url = apiHost + '/v1/integer-spiral?seed='+input+'&spin='+spin;
      return $http(getRequest(url));
    }


    function getRequest(url) {
      return {
       method: 'GET',
       url: url,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Accept': 'plain/text'
        }
      }
    }
  }

})();
