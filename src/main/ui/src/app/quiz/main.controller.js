(function() {
  'use strict';

  angular
    .module('puzzlesQuizUi')
    .controller('QuizController', QuizController);

  /** @ngInject */
  function QuizController() {

    var vm = this;
    vm.data = {
      selectedIndex: 0,
      bottom:        false
    };
  }

})();
