/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */

'use strict';

var MainPage = function() {
  this.tabItems = element.all(by.css('md-tab-item'));
  this.integerSpiralTab = this.tabItems.get(0);

  this.isInputEl = element(by.css('.integer-spiral input.number'));
  this.isSpinEl = element(by.css('.integer-spiral md-select.spin'));
  this.isSubmitEl = element(by.css('.integer-spiral button.submit'));
  this.isOutputEl = element(by.css('.integer-spiral div.output'));
};
module.exports = new MainPage();
