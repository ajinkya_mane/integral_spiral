'use strict';

describe('The main view', function () {
  var page;

  beforeEach(function () {
    browser.get('/index.html');
    page = require('./main.po');
  });

  it('should give integer spiral', function() {
    page.integerSpiralTab.click();
    page.isInputEl.sendKeys('0');
    //element(by.cssContainingText('md-option', 'right')).click();
    page.isSubmitEl.click();
    expect(page.isOutputEl.getText()).toBe('0');
  });

});
