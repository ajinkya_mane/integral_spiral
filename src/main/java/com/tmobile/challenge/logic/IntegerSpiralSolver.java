package com.tmobile.challenge.logic;

import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Shrimant_Metre on 9/14/2017.
 */
public class IntegerSpiralSolver extends AbstractSolver {

    @Override
    protected void validate(Map<String, String> input) {

        if (input.containsKey("input.seed")) {
            String seed = input.get("input.seed");
            try {
                Integer.parseInt(seed);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Error:Input is invalid");
            }
        } else {
            throw new IllegalArgumentException("Error:Input number is not available");
        }

        if (input.containsKey("input.spin")) {
            //
        } else {
            throw new IllegalArgumentException("Error:Input spin is not available");
        }
    }

    @Override
    protected void execute(Map<String, String> input) {
        String seed = input.get("input.seed");
        String spinStr = input.get("input.spin");

        int iSeed = Integer.parseInt(seed);
        boolean spin = spinStr.equalsIgnoreCase("left");
        String solution = process(iSeed, spin);
        input.put("solution", solution);
    }

    private String process(int iSeed, boolean spin) {
        // determining the matrix size
        int matrixSize = getMatrixSize(iSeed);

        spiral = new int[matrixSize][matrixSize];
        // assigning -1 for empty array values.
        for (int[] row : spiral)
            Arrays.fill(row, -1);

        // initial position
        rowIndex = colIndex = (matrixSize / 2);
        spiral[rowIndex][colIndex] = 0;

        turn(iSeed, spin);
        return toStringSpiral(spiral);
    }

    private void turn(int iSeed, boolean spin) {
        int i = 0;
        while (i++ < iSeed) {
            switch (currentDir) {
                case NORTH:
                    if (spin) {
                        navigateWest(spin);
                    } else {
                        navigateEast(spin);
                    }
                    break;
                case SOUTH:
                    if (spin) {
                        navigateEast(spin);
                    } else {
                        navigateWest(spin);
                    }
                    break;
                case WEST:
                    if (spin) {
                        navigateSouth(spin);
                    } else {
                        navigateNorth(spin);
                    }
                    break;
                case EAST:
                    if (spin) {
                        navigateNorth(spin);
                    } else {
                        navigateSouth(spin);
                    }
                    break;
                default:
                    throw new RuntimeException("Invalid direction");
            }
            spiral[rowIndex][colIndex] = i;
        }
    }

    private String toStringSpiral(int[][] spiral) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < spiral.length; i++) {
            for (int j = 0; j < spiral[i].length; j++) {
                if (spiral[i][j] == -1) {
                    builder.append("   ");
                } else {
                    builder.append(String.format("%3d", spiral[i][j]));
                }
                if(j != spiral[i].length - 1) {
                    builder.append(":");
                }
            }
            builder.append(",");
        }
        if(builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    private int getMatrixSize(int iSeed) {
        int matrixSize = 1;
        while (iSeed >= matrixSize * matrixSize) {
            matrixSize = matrixSize + 2;
        }
        return matrixSize;
    }

    private void navigateNorth(boolean spin) {
        rowIndex--;
        if (spiral[rowIndex][colIndex] == -1) {
            currentDir = Direction.NORTH;
        } else {
            rowIndex++;
            colIndex = spin ? colIndex + 1 : colIndex - 1;
        }
    }

    private void navigateWest(boolean spin) {
        colIndex--;
        if (spiral[rowIndex][colIndex] == -1) {
            currentDir = Direction.WEST;
        } else {
            colIndex++;
            rowIndex = spin ? rowIndex - 1 : rowIndex + 1;
        }
    }

    private void navigateSouth(boolean spin) {
        rowIndex++;
        if (spiral[rowIndex][colIndex] == -1) {
            currentDir = Direction.SOUTH;
        } else {
            rowIndex--;
            colIndex = spin ? colIndex - 1 : colIndex + 1;
        }
    }

    private void navigateEast(boolean spin) {
        colIndex++;
        if (spiral[rowIndex][colIndex] == -1) {
            currentDir = Direction.EAST;
        } else {
            colIndex--;
            rowIndex = spin ? rowIndex + 1 : rowIndex - 1;
        }
    }

    enum Direction {
        NORTH, SOUTH, WEST, EAST;
    }

    private int rowIndex;
    private int colIndex;
    private Direction currentDir = Direction.NORTH;
    private int[][] spiral;

}
