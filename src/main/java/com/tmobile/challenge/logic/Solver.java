package com.tmobile.challenge.logic;

import java.util.Map;

/**
 * Created by Ajinkya C. Mane on 9/18/2017.
 */
public interface Solver {

    void solve(Map<String, String> input);

}
