package com.tmobile.challenge.logic;

import java.util.Map;

/**
 * Created by Ajinkya C. Mane on 9/18/2017.
 */
public abstract class AbstractSolver implements Solver {

    protected abstract void validate(Map<String, String> input);

    protected abstract void execute(Map<String, String> input);

    public void solve(Map<String, String> input) {
        validate(input);
        execute(input);
    }

}
