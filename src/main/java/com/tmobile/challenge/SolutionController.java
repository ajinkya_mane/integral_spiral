package com.tmobile.challenge;

import com.tmobile.challenge.logic.IntegerSpiralSolver;
import com.tmobile.challenge.logic.Solver;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * exposes the integer spiral microservice's endpoints
 */
@RestController
@RequestMapping(value = "/v1")
@CrossOrigin(origins = "http://localhost:3000")
public class SolutionController implements SolutionApi {
    @RequestMapping(value = "/integer-spiral", method = RequestMethod.GET)
    public String getIntegerSpiral(@RequestParam("seed") String seed, @RequestParam("spin") String spin) {

        Map<String, String> input = new HashMap<String, String>();
        input.put("input.seed", seed);
        input.put("input.spin", spin);

        Solver solver = new IntegerSpiralSolver();
        solver.solve(input);

        return input.get("solution");
    }

    @ExceptionHandler
    void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

}
