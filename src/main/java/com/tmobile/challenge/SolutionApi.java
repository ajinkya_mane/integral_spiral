package com.tmobile.challenge;

/**
 * Created by Ajinkya C. Mane on 9/18/2017.
 */
public interface SolutionApi {


    String getIntegerSpiral(String seed, String spin);


}
